# Hugo Theme for the TU-Darmstadt Coporate Design (2019 Edition)

## Usage
Create a new site with `hugo new site <sitename>` and add the following into the config.toml:
```toml
[module]
  [[module.imports]]
  path = "git.rwth-aachen.de/isp/shk/tudatheme"
```
Afterwards initialize the hugo mod support with `hugo mod init <sitename>` (you can use something different than the sitename, but this is more consistent).
The page can then be build with `hugo` or be served using the development server with `hugo serve`.